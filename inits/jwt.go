package inits

// @Author ly
// @Date 2023/11/30 11:15:00
// @Desc

import (
	"errors"
	"fmt"
	"gin/domain/common"
	"gin/domain/system"
	"gin/global"
	system2 "gin/service/system"
	"gin/utils"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	"github.com/google/uuid"
	"net/http"
	"time"
)

type JWT struct {
	// 声明签名信息
	Secret []byte
}

// NewJWT 初始化JWT对象
func NewJWT() *JWT {
	return &JWT{
		Secret: []byte(global.Settings.JwtConfig.Secret),
	}
}

// CustomClaims 自定义有效载荷
type CustomClaims struct {
	Name               string        `json:"name"`
	UserId             int           `json:"userId"`
	Id                 string        `json:"id"`
	FuncArray          []string      `json:"funcArray"`
	Info               []system.Menu `json:"info"`
	Role               string        `json:"role"`
	jwt.StandardClaims               // StandardClaims结构体实现了Claims接口(Valid()函数)
}

// CreateToken 调用jwt-go库生成token
func (j *JWT) CreateToken(claims CustomClaims) (string, error) {
	// 指定编码算法为jwt.SigningMethodHS256
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims) // 返回一个token结构体指针
	return token.SignedString(j.Secret)
}

// ParserToken token解码
func (j *JWT) ParserToken(tokenString string) (*CustomClaims, error) {
	// 输入用户自定义的Claims结构体对象,token,以及自定义函数来解析token字符串为jwt的Token结构体指针
	//Keyfunc是匿名函数类型: type Keyfunc func(*Token) (interface{}, error)
	//func ParseWithClaims(tokenString string, claims Claims, keyFunc Keyfunc) (*Token, error) {}
	token, err := jwt.ParseWithClaims(tokenString, &CustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		return j.Secret, nil
	})
	if err != nil {
		var ve *jwt.ValidationError
		if errors.As(err, &ve) { // jwt.ValidationError：是一个无效token的错误结构
			if ve.Errors&jwt.ValidationErrorMalformed != 0 { // ValidationErrorMalformed是一个uint常量，表示token不可用
				return nil, fmt.Errorf("token不可用")
			} else if ve.Errors&jwt.ValidationErrorExpired != 0 { // ValidationErrorExpired表示Token过期
				return nil, fmt.Errorf("token过期")
			} else if ve.Errors&jwt.ValidationErrorNotValidYet != 0 { // ValidationErrorNotValidYet表示无效token
				return nil, fmt.Errorf("无效的token")
			} else {
				return nil, fmt.Errorf("token不可用")
			}
		}
		return nil, err
	}
	// 将token中的claims信息解析出来并断言成用户自定义的有效载荷结构
	claims, ok := token.Claims.(*CustomClaims)
	if ok && token.Valid {
		return claims, nil
	}
	return nil, fmt.Errorf("token不可用")
}

// JWTAuth 定义一个JWTAuth的中间件
func JWTAuth() gin.HandlerFunc {
	return func(c *gin.Context) {
		// 通过http header中的token解析来认证
		token := c.Request.Header.Get("token")
		if token == "" {
			utils.ReturnError(c, http.StatusForbidden, "请求未携带token，无权访问！")
			c.Abort() // Abort 函数在被调用的函数中阻止后续中间件的执行(这里都没有携带token，后续就不用执行了)
			return
		}
		j := NewJWT()                       // 初始化一个JWT对象实例，并根据结构体方法来解析token
		claims, err := j.ParserToken(token) // 解析token中包含的相关信息（有效载荷）
		if err != nil {
			utils.ReturnError(c, http.StatusForbidden, err.Error())
			c.Abort()
			return
		}
		//fmt.Println(claims.Id)
		if redistoken, _ := global.Redis.Get(common.LoginTokenKey + fmt.Sprint(claims.UserId) + claims.Name).Result(); redistoken != "" {
			//if redistoken, _ := global.Redis.Get(common.LoginTokenKey + claims.Id).Result(); redistoken != "" {
			if redistoken == token {
				if refresh, _ := global.Redis.Get(common.RefreshToken + fmt.Sprint(claims.UserId) + claims.Name).Result(); refresh == "" {
					url := c.Request.URL.Path
					//if strings.HasPrefix(url, "/api/") {
					//	if c.Request.Method == http.MethodPost {
					//		url += "/" + c.PostForm("d")
					//	} else {
					//		url += "/" + c.Query("d")
					//	}
					//}
					if url != "/logout" {
						if claims.Role != "1" {
							if !PermissionAuth(claims.FuncArray, url) {
								utils.ReturnError(c, http.StatusInternalServerError, "您没有该权限")
								c.Abort()
								return
							}
						}
					}
					//若在到期之前时间段请求可刷新token
					if claims.ExpiresAt-time.Now().Unix() < global.Settings.JwtConfig.RefreshTokenPeriod {
						global.Redis.Set(common.RefreshToken+fmt.Sprint(claims.UserId)+claims.Name, 1, time.Duration(2)*time.Second)
						c.Header("refresh_token", GetToken(c, claims.Name, claims.UserId))
					}
					// 将解析后的有效载荷claims重新写入gin.Context引用对象中（gin的上下文）
					c.Set("claims", claims)
				}
			} else {
				utils.ReturnError(c, http.StatusForbidden, "token已刷新,请使用最新token")
				c.Abort()
				return
			}
		} else {
			utils.ReturnError(c, http.StatusForbidden, "token不可用")
			c.Abort()
			return
		}
	}
}
func GetToken(c *gin.Context, user string, userId int) string {
	id := uuid.New().String()
	j := NewJWT() // 构造SignKey: 签名和解签名需要使用一个值
	var (
		funcArray []string
		menu      []system.Menu
		role      string
	)
	global.DB.Raw("select role_id from user_role  ru left join user u on ru.user_id=u.id where u.username=? and id=?", user, userId).Scan(&role)
	if role == "1" {
		global.DB.Raw("select DISTINCT url from menu where url is not null and not url=''").Scan(&funcArray)
		menu = system2.GetMenuTree(nil)
	} else {
		//查询全部权限存入数组 权限对比即可
		global.DB.Raw("select DISTINCT m.url from user u join user_role  r on u.id=r.user_id left join role_menu p on r.role_id=p.role_id left join menu m on p.menu_id=m.menu_id where u.username=? and url is not null and not url='' and u.id=?", user, userId).Scan(&funcArray)
		global.DB.Raw("select m.menu_id,m.menu_name,m.pid,m.order_num,m.path,m.component,m.menu_type,m.url,m.remark,m.icon from user u left join user_role  ru on u.id=ru.user_id left join role_menu rm on ru.role_id=rm.role_id left join menu m on m.menu_id=rm.menu_id where u.username=? and u.id=? order by pid,m.order_num", user, userId).Scan(&menu)
		if !(len(menu) == 1 && menu[0].MenuId == 0) {
			menu = system2.GetMenuTrees(menu, menu[0].Pid)
		}
	}
	claims := CustomClaims{ // 构造用户claims信息（负载）
		Id:        id,
		UserId:    userId,
		Name:      user,
		FuncArray: funcArray,
		Info:      menu,
		Role:      role,
		StandardClaims: jwt.StandardClaims{
			// 签名生效时间秒
			NotBefore: int64(time.Now().Unix()),
			// 签名过期时间秒
			ExpiresAt: int64(time.Now().Unix() + global.Settings.JwtConfig.Expire),
			// 签名颁发者
			Issuer: global.Settings.JwtConfig.Secret,
		},
	}

	token, err := j.CreateToken(claims) // 根据claims生成token对象
	if err != nil {
		panic(err)
	}
	//存redis
	//result, err := global.Redis.Set(common.LoginTokenKey+id, token, time.Duration(global.Settings.JwtConfig.Expire)*time.Second).Result()
	c.Set("claims", claims)
	//result, err := global.Redis.Set(common.LoginTokenKey+id, token, -1).Result()
	global.Redis.Del(common.LoginTokenKey + fmt.Sprint(userId) + user)
	result, err := global.Redis.Set(common.LoginTokenKey+fmt.Sprint(userId)+user, token, time.Duration(global.Settings.JwtConfig.Expire)*time.Second).Result()
	fmt.Println(result)
	return token
}
func PermissionAuth(funcArray []string, url string) (flag bool) {
	for _, s := range funcArray {
		if s == url {
			flag = true
		}
	}
	return flag
}
