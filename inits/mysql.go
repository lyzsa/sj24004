package inits

import (
	"fmt"
	"gin/global"
	"gopkg.in/natefinch/lumberjack.v2"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"io"
	"log"
	"os"
	"time"
)

func InitMysqlDB() {
	mysqlInfo := global.Settings.Mysqlinfo
	// 参考 https://github.com/go-sql-driver/mysql#dsn-data-source-name 获取详情
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		mysqlInfo.Name, mysqlInfo.Password, mysqlInfo.Host,
		mysqlInfo.Port, mysqlInfo.DBName)
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		Logger: getGormLogger(),
	})
	if err != nil {
		fmt.Println("打开连接失败")
	}
	sqlDB, err := db.DB()
	if err != nil {
		fmt.Println("无法连接到数据库")
	}
	// 设置连接池配置
	sqlDB.SetMaxOpenConns(100)          // 设置最大打开连接数
	sqlDB.SetMaxIdleConns(10)           // 设置连接池中的最大闲置连接数
	sqlDB.SetConnMaxLifetime(time.Hour) // 设置连接的最大复用时间
	global.DB = db
}
func getGormLogger() logger.Interface {
	var logMode logger.LogLevel

	switch global.Settings.Mysqlinfo.LogMode {
	case "silent":
		logMode = logger.Silent
	case "error":
		logMode = logger.Error
	case "warn":
		logMode = logger.Warn
	case "info":
		logMode = logger.Info
	default:
		logMode = logger.Info
	}
	return logger.New(getGormLogWriter(), logger.Config{
		SlowThreshold:             200 * time.Millisecond, // 慢 SQL 阈值
		LogLevel:                  logMode,                // 日志级别
		IgnoreRecordNotFoundError: false,                  // 忽略ErrRecordNotFound（记录未找到）错误
	})
}

// 自定义 gorm Writer
func getGormLogWriter() logger.Writer {
	var writer io.Writer
	// 是否启用日志文件
	if global.Settings.Mysqlinfo.EnableFileLogWriter {
		// 自定义 Writer
		writer = &lumberjack.Logger{
			Filename:   global.Settings.Logs + "/" + time.Now().Format(time.DateOnly) + "-" + global.Settings.Mysqlinfo.Logfile,
			MaxSize:    global.Settings.LogConfig.MaxSize,
			MaxBackups: global.Settings.LogConfig.MaxBackups,
			MaxAge:     global.Settings.LogConfig.MaxAge,
			Compress:   global.Settings.LogConfig.Compress,
		}
	} else {
		// 默认 Writer
		writer = os.Stdout
	}
	writer = io.MultiWriter(writer, os.Stdout)
	return log.New(writer, "\r\n", log.LstdFlags)
}
