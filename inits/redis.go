package inits

import (
	"errors"
	"fmt"
	"gin/global"
	"github.com/go-redis/redis"
	"strconv"
	"time"
)

func InitRedis() {
	redisInfo := global.Settings.RedisInfo
	global.Redis = redis.NewClient(&redis.Options{
		Addr:         redisInfo.Host + ":" + strconv.Itoa(redisInfo.Port),
		Password:     redisInfo.Password,
		DB:           redisInfo.Db,
		DialTimeout:  10 * time.Second,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
		PoolSize:     10,
		PoolTimeout:  30 * time.Second,
	})
	ping, err := global.Redis.Ping().Result()
	if errors.Is(err, redis.Nil) {
		fmt.Print("Redis异常")
	} else if err != nil {
		fmt.Print("失败:", err)
	} else {
		fmt.Print("redis连接状态：" + ping + "\n")
	}
}
