package inits

import (
	"database/sql"
	"fmt"
	"gin/global"
	_ "github.com/taosdata/driver-go/v3/taosRestful"
)

func InitTaos() {
	taosInfo := global.Settings.Taosinfo
	// 参考 https://github.com/go-sql-driver/mysql#dsn-data-source-name 获取详情
	dsn := fmt.Sprintf("%s:%s@http(%s:%d)/",
		taosInfo.Name, taosInfo.Password, taosInfo.Host,
		taosInfo.Port)
	global.Taos, _ = sql.Open("taosRestful", dsn)
}
