package inits

// @Author ly
// @Date 2024/1/2 16:05:00
// @Desc

import (
	"database/sql"
	"fmt"
	"gin/global"
	_ "github.com/denisenkom/go-mssqldb"
)

func InitSqlServer() {
	sqlServerInfo := global.Settings.SqlServerInfo
	// 参考 https://github.com/go-sql-driver/mysql#dsn-data-source-name 获取详情
	dsn := fmt.Sprintf("sqlserver://%s:%s@%s:%d?database=%s&encrypt=disable", sqlServerInfo.Name, sqlServerInfo.Password, sqlServerInfo.Host, sqlServerInfo.Port, sqlServerInfo.DBName)
	//dsn := fmt.Sprintf("server=%s;user id=%s;password=%s;port=%d;database=%s;", sqlServerInfo.Host, sqlServerInfo.Name, sqlServerInfo.Password, sqlServerInfo.Port, sqlServerInfo.DBName)
	global.Ssql, _ = sql.Open("sqlserver", dsn)
}
