package inits

// @Author ly
// @Date 2024/1/5 9:27:00
// @Desc

import (
	"fmt"
	"gin/domain/common"
	"gin/global"
	"gin/utils"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gopkg.in/natefinch/lumberjack.v2"
	"io"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"runtime/debug"
	"time"
)

func Log() gin.HandlerFunc {
	return func(c *gin.Context) {
		if _, err := os.Stat(global.Settings.Logs); os.IsNotExist(err) {
			err = os.MkdirAll(global.Settings.Logs, 0777)
			if err != nil {
				panic(fmt.Errorf("create log dir '%s' error: %s", global.Settings.Logs, err))
			}
		}
		timeStr := time.Now().Format(time.DateOnly)
		fileName := path.Join(global.Settings.Logs, timeStr+".log")
		os.Stderr, _ = os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)
		//实例化
		logger := logrus.New()
		//设置输出
		logger.Out = os.Stderr
		//设置日志级别
		logger.SetLevel(logrus.InfoLevel)
		//打印方法
		//logger.SetReportCaller(true)
		//设置日志格式
		logger.SetFormatter(&logrus.TextFormatter{TimestampFormat: time.DateTime})
		// 开始时间
		startTime := time.Now()
		// 处理请求
		c.Next()
		// 结束时间
		endTime := time.Now()
		// 执行时间
		latencyTime := endTime.Sub(startTime)
		method := c.Request.Method
		uri := c.Request.RequestURI
		uri, _ = url.QueryUnescape(uri)
		form := utils.EncodePostForm(c.Request.PostForm)
		statusCode := c.Writer.Status()
		clientIP := utils.GetRemoteClientIp(c.Request)
		errormessage := c.Errors.ByType(gin.ErrorTypePrivate).String()
		// 日志格式
		if len(c.Errors) > 0 {
			logger.Errorf(errormessage)
		} else {
			msg := fmt.Sprintf(" %s  %s  %s  %s  %d  %s  %s", clientIP, method, uri, form, statusCode, latencyTime, errormessage)
			if statusCode == http.StatusOK {
				logger.Info(msg)
			} else {
				logger.Error(msg)
			}
		}
		if method == http.MethodPost && c.Request.URL.Path != "/login" {
			claims, _ := c.Get("claims")
			var username string
			if claims != nil {
				username = claims.(*CustomClaims).Name
			} else {
				username = "未登录"
			}
			global.DB.Exec("insert into opera_log(url, statuscode, ip, username, createtime, param) values(?,?,?,?,?,?)", uri, statusCode, clientIP, username, utils.GetDate(), form)
		}
	}
}
func Write(msg string) {
	setOutPutFile(logrus.InfoLevel, "info")
	logrus.Info(msg)
}

func Debug(fields logrus.Fields, args ...interface{}) {
	setOutPutFile(logrus.DebugLevel, "debug")
	logrus.WithFields(fields).Debug(args)
}

func Info(fields logrus.Fields, args ...interface{}) {
	setOutPutFile(logrus.InfoLevel, "info")
	logrus.WithFields(fields).Info(args)
}

func Warn(fields logrus.Fields, args ...interface{}) {
	setOutPutFile(logrus.WarnLevel, "warn")
	logrus.WithFields(fields).Warn(args)
}

// 执行完程序结束
func Fatal(fields logrus.Fields, args ...interface{}) {
	setOutPutFile(logrus.FatalLevel, "fatal")
	logrus.WithFields(fields).Fatal(args)
}

func Error(fields logrus.Fields, args ...interface{}) {
	setOutPutFile(logrus.ErrorLevel, "error")
	logrus.WithFields(fields).Error(args)
}

func Panic(fields logrus.Fields, args ...interface{}) {
	setOutPutFile(logrus.PanicLevel, "panic")
	logrus.WithFields(fields).Panic(args)
}

func Trace(fields logrus.Fields, args ...interface{}) {
	setOutPutFile(logrus.TraceLevel, "trace")
	logrus.WithFields(fields).Trace(args)
}
func setOutPutFile(level logrus.Level, logName string) {
	if _, err := os.Stat(global.Settings.Logs); os.IsNotExist(err) {
		err = os.MkdirAll(global.Settings.Logs, 0777)
		if err != nil {
			panic(fmt.Errorf("create log dir '%s' error: %s", global.Settings.Logs, err))
		}
	}

	timeStr := time.Now().Format(time.DateOnly)
	fileName := path.Join(global.Settings.Logs, logName+"_"+timeStr+".log")
	var err error
	os.Stderr, err = os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		fmt.Println("open log file err", err)
	}
	var writer io.Writer
	writer = &lumberjack.Logger{
		MaxSize:    global.Settings.LogConfig.MaxSize,
		MaxBackups: global.Settings.LogConfig.MaxBackups,
		MaxAge:     global.Settings.LogConfig.MaxAge,
		Compress:   global.Settings.LogConfig.Compress,
	}
	writer = io.MultiWriter(writer, os.Stderr)
	logrus.SetOutput(writer)
	logrus.SetLevel(level)
	logrus.SetFormatter(&logrus.TextFormatter{TimestampFormat: time.DateTime})
	return
}
func Recover(c *gin.Context) {
	defer func() {
		if err := recover(); err != nil {
			if _, errDir := os.Stat(global.Settings.Logs); os.IsNotExist(errDir) {
				errDir = os.MkdirAll(global.Settings.Logs, 0777)
				if errDir != nil {
					panic(fmt.Errorf("create log dir '%s' error: %s", global.Settings.Logs, errDir))
				}
			}
			timeStr := time.Now().Format(time.DateOnly)
			fileName := path.Join(global.Settings.Logs, timeStr+".log")
			f, errFile := os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)
			if errFile != nil {
				fmt.Println(errFile)
			}
			timeFileStr := time.Now().Format(time.DateTime)
			_, _ = f.WriteString("panic error time:" + timeFileStr + "\n")
			_, _ = f.WriteString(fmt.Sprintf("%v", err))
			_, file, line, _ := runtime.Caller(1)
			file = filepath.Base(file)
			stackTrace := fmt.Sprintf("%s:%d", file, line)
			_, _ = f.WriteString(stackTrace + "\n")
			_ = f.Close()
			debug.PrintStack()
			utils.ReturnError(c, http.StatusInternalServerError, fmt.Sprintf("%v", err))
			uri := c.Request.RequestURI
			uri, _ = url.QueryUnescape(uri)
			if uri == "/login" && err != common.DatabaseConnectFail {
				global.DB.Exec("insert into login_log(username,ip,msg,logintime) values(?,?,?,?)", c.PostForm("username"), utils.GetRemoteClientIp(c.Request), err, utils.GetDate())
			}
			//终止后续接口调用，不加的话recover到异常后，还会继续执行接口里后续代码
			c.Abort()
		}
	}()
	c.Next()
}
