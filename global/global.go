package global

import (
	"database/sql"
	"fmt"
	"gin/domain/common"
	"github.com/go-redis/redis"
	"gorm.io/gorm"
	"math"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// 全局变量
var (
	Settings common.ServerConfig
	DB       *gorm.DB
	Redis    *redis.Client
	Taos     *sql.DB
	Ssql     *sql.DB
)

func Results(rows *sql.Rows) (result []map[string]interface{}) {
	for rows.Next() {
		columns, _ := rows.Columns()
		scanArgs := make([]interface{}, len(columns))
		values := make([]interface{}, len(columns))
		re := regexp.MustCompile(`\b(last|avg)\(`)
		for i := range values {
			scanArgs[i] = &values[i]
			columns[i] = re.ReplaceAllString(columns[i], "")
			columns[i] = strings.TrimRight(columns[i], ")")
			//columns[i] = strings.Replace(strings.Replace(columns[i], "last(", "", -1), ")", "", -1)
		}
		//将数据保存到 record 字典
		_ = rows.Scan(scanArgs...)
		record := make(map[string]interface{})
		for i, col := range values {
			if col != nil {
				if reflect.TypeOf(col).String() != "string" {
					if _, ok := col.(time.Time); ok {
						record[columns[i]] = fmt.Sprintf("%v", col)
					} else if value, ok := col.(float32); ok {
						record[columns[i]] = fmt.Sprintf("%.2f", value)
					} else if _, ok := col.(float64); ok {
						record[columns[i]] = fmt.Sprintf("%.2f", math.Floor((col.(float64))*100)/100)
					} else {
						record[columns[i]] = fmt.Sprintf("%v", col)
					}
				} else {
					record[columns[i]] = fmt.Sprintf("%v", col)
				}
			}
		}
		result = append(result, record)
	}
	for i := 0; i < len(result); i++ {
		if result[i]["ts"] != nil {
			result[i]["ts"] = GetTdtime(fmt.Sprintf("%v", result[i]["ts"]), 0)
		}
	}
	return
}

// td时间转换
func GetTdtime(ts string, f int) string {
	local := time.FixedZone("CST", 3600*8)
	t, _ := time.Parse("2006-01-02 15:04:05.999 -0700 MST", ts)
	t = t.In(local)
	//  转化为北京时间
	year := strconv.Itoa(t.Year())
	month := ZeroFillByStr(strconv.Itoa(int(t.Month())), 2)
	day := ZeroFillByStr(strconv.Itoa(t.Day()), 2)
	hour := ZeroFillByStr(strconv.Itoa(t.Hour()), 2)
	minute := ZeroFillByStr(strconv.Itoa(t.Minute()), 2)
	second := ZeroFillByStr(strconv.Itoa(t.Second()), 2)
	result := year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second
	if f != 0 {
		millisecond := ZeroFillByStr(strconv.Itoa(t.Nanosecond()/1e6), 3)
		result = result + "." + millisecond
	}
	return result
}

// 补0
func ZeroFillByStr(str string, l int) string {
	result := str
	if len(str) != l {
		for i := len(str); i < l; i++ {
			result = "0" + result
		}
	}
	return result
}
