package common

/* @Author ly
** @Date 2024/5/15 8:44:00
** @Desc
 */

type Param struct {
	Table    string `form:"table"`
	Key      string `form:"key"`
	Value    string `form:"value"`
	Cond     string `form:"cond"`
	Val      string `form:"val"`
	VeryCond string `form:"veryCond"`
	VeryConn string `form:"veryConn"`
	VeryVal  string `form:"veryVal"`
	DescCond string `form:"descCond"`
	AscCond  string `form:"ascCond"`
	Data     string `form:"data"`
}

func (c Param) TableIsNull() {
	if c.Table == "" {
		panic(TableNotIsNull)
	}
	if (c.Cond == "" && c.Val != "") || (c.Cond != "" && c.Val == "") || (c.VeryCond != "" && c.VeryVal == "") || (c.VeryCond == "" && c.VeryVal != "") || (c.VeryCond != "" && c.VeryVal != "" && c.VeryConn == "") || (c.VeryCond == "" && c.VeryVal == "" && c.VeryConn != "") {
		panic(ParamLengthNotMatch)
	}
}

type CondTime struct {
	Starttime string `form:"startTime"`
	Endtime   string `form:"endTime"`
}
