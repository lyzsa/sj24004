package common

/* @Author ly
** @Date 2024/5/18 9:14:00
** @Desc
 */

var (
	LoginTokenKey = "login_tokens:"
	RefreshToken  = "refresh_tokens:"
)
