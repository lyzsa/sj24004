package common

/* @Author ly
** @Date 2024/7/12 11:47:00
** @Desc
 */
type Api struct {
	T string `json:"t" form:"t"`
	W string `json:"w" form:"w"`
	O string `json:"o" form:"o"`
	L string `json:"l" form:"l"`
	C string `json:"c" form:"c"`
	D string `json:"d" form:"d"`
}
