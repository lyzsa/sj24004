package common

type ServerConfig struct {
	Name          string          `mapstructure:"name"`
	Port          int             `mapstructure:"port"`
	Static        string          `mapstructure:"static"`
	Mysqlinfo     MysqlConfig     `mapstructure:"mysql"`
	RedisInfo     RedisConfig     `mapstructure:"redis"`
	Logs          string          `mapstructure:"logs"`
	Taosinfo      TaosConfig      `mapstructure:"taos"`
	Uploadurl     string          `mapstructure:"uploadurl"`
	JwtConfig     JwtConfig       `mapstructure:"jwt"`
	LogConfig     LogConfig       `mapstructure:"log"`
	SqlServerInfo SqlServerConfig `mapstructure:"sqlserver"`
	Mode          string          `mapstructure:"mode"`
}
type MysqlConfig struct {
	Host                string `mapstructure:"host"`
	Port                int    `mapstructure:"port"`
	Name                string `mapstructure:"name"`
	Password            string `mapstructure:"password"`
	DBName              string `mapstructure:"dbName"`
	Logfile             string `mapstructure:"logfile"`
	LogMode             string `mapstructure:"logmode"`
	EnableFileLogWriter bool   `mapstructure:"enable_file_log_writer"`
}
type TaosConfig struct {
	Host     string `mapstructure:"host"`
	Port     int    `mapstructure:"port"`
	Name     string `mapstructure:"name"`
	Password string `mapstructure:"password"`
}
type RedisConfig struct {
	Host     string `mapstructure:"host"`
	Port     int    `mapstructure:"port"`
	Password string `mapstructure:"password"`
	Db       int    `mapstructure:"db"`
}
type JwtConfig struct {
	Secret             string `mapstructure:"secret"`
	RefreshTokenPeriod int64  `mapstructure:"refresh_token_period"`
	Expire             int64  `mapstructure:"expire"`
	Super              string `mapstructure:"super"`
}
type LogConfig struct {
	MaxSize    int  `mapstructure:"maxsize"`
	MaxBackups int  `mapstructure:"maxbackups"`
	MaxAge     int  `mapstructure:"maxage"`
	Compress   bool `mapstructure:"compress"`
}
type SqlServerConfig struct {
	Host     string `mapstructure:"host"`
	DBName   string `mapstructure:"dbname"`
	Name     string `mapstructure:"name"`
	Password string `mapstructure:"password"`
	Port     int    `mapstructure:"port"`
}
