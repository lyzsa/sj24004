package common

/* @Author ly
** @Date 2024/5/18 8:59:00
** @Desc
 */

var (
	ParamLengthNotMatch  = "参数长度不匹配"
	ParamFormatError     = "参数格式化错误"
	ParamNoFull          = "参数不完整"
	TableNotIsNull       = "表名不可为空"
	CaptchaExpire        = "验证码已失效"
	CaptchaError         = "验证码错误"
	UserPasswordNotMatch = "用户名密码错误"
	UserNoExist          = "用户不存在"
	UserExist            = "用户名已存在"
	RoleExist            = "角色名称或权限字符已存在"
	PostExist            = "岗位名称已存在"
	DeptExist            = "部门名称已存在"
	MenuNameExist        = "菜单名称已存在"
	UserOrPasswdNotNUll  = "用户名或密码不可为空且用户名长度需大于等于2"
	UserNotNUll          = "用户名不可为空且长度需大于等于2"
	GetCaptchaBad        = "获取验证码失败"
	GetCaptchaError      = "获取验证码错误"
	AdminNotAllowOperate = "超级管理员不允许操作"
	DatabaseConnectFail  = "数据库连接失败"
)
