package system

/* @Author ly
** @Date 2024/5/14 9:16:00
** @Desc
 */

import (
	"gin/utils"
	"gorm.io/gorm"
)

type User struct {
	Id       int    `json:"id" form:"id" gorm:"primary_key"`
	Username string `json:"username" form:"username"`
	//Nickname   string `json:"nickname" form:"nickname"`
	Sex        int    `json:"sex" form:"sex"`
	Email      string `json:"email" form:"email"`
	Passwd     string `json:"passwd" form:"passwd"`
	Phone      string `json:"phone" form:"phone"`
	Status     string `json:"status" form:"status"`
	RoleIds    string `json:"roleIds" form:"roleIds"`
	RoleName   string `json:"roleName" form:"roleName"`
	LoginIp    string `json:"loginIp"`
	LoginTime  string `json:"loginTime"`
	CreateTime string `json:"createTime" form:"createTime"`
}

func (User) TableName() string {
	return "user"
}
func (u *User) AfterFind(*gorm.DB) (err error) {
	u.CreateTime = utils.Gettime(u.CreateTime)
	return
}
