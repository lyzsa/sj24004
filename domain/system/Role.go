package system

/* @Author ly
** @Date 2024/5/20 11:50:00
** @Desc
 */

import (
	"gin/utils"
	"gorm.io/gorm"
)

type Role struct {
	RoleId      int    `json:"roleId" form:"roleId" gorm:"primary_key"`
	RoleName    string `json:"roleName" form:"roleName"`
	RoleKey     string `json:"roleKey" form:"roleKey"`
	RoleSort    int    `json:"roleSort" form:"roleSort"`
	CreateTime  string `json:"createTime" form:"createTime"`
	Permissions string `json:"permissions" form:"permissions" gorm:"-"`
}

func (Role) TableName() string {
	return "role"
}
func (r *Role) AfterFind(*gorm.DB) (err error) {
	r.CreateTime = utils.Gettime(r.CreateTime)
	return
}
