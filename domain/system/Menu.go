package system

/* @Author ly
** @Date 2024/5/20 8:52:00
** @Desc
 */

type Menu struct {
	MenuId    int    `json:"menuId" form:"menuId" gorm:"primary_key"`
	MenuName  string `json:"menuName" form:"menuName"`
	Pid       int    `json:"pid" form:"pid"`
	OrderNum  int    `json:"orderNum" form:"orderNum"`
	Path      string `json:"path" form:"path"`
	Component string `json:"component" form:"component"`
	MenuType  string `json:"menuType" form:"menuType"`
	Url       string `json:"url" form:"url"`
	Remark    string `json:"remark" form:"remark"`
	Icon      string `json:"icon" form:"icon"`
	Children  []Menu `json:"children,omitempty" gorm:"-"`
	IsUse     bool   `json:"-" gorm:"-"`
}

func (Menu) TableName() string {
	return "menu"
}
