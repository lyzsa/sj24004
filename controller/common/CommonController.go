package common

/* @Author ly
** @Date 2024/5/15 8:40:00
** @Desc
 */

import (
	"gin/service/common"
	"gin/utils"
	"github.com/gin-gonic/gin"
	"net/http"
)

type Controller struct{}

func (co Controller) CommonList(c *gin.Context) {
	data := common.List(c)
	utils.ReturnData(c, data)
}
func (co Controller) CommonAdd(c *gin.Context) {
	msg, err := common.Add(c)
	if err != nil {
		utils.ReturnError(c, http.StatusInternalServerError, msg)
	} else {
		utils.ReturnOk(c, msg)
	}
}
func (co Controller) CommonModify(c *gin.Context) {
	msg, err := common.Modify(c)
	if err != nil {
		utils.ReturnError(c, http.StatusInternalServerError, msg)
	} else {
		utils.ReturnOk(c, msg)
	}
}
func (co Controller) CommonDel(c *gin.Context) {
	msg, err := common.Del(c)
	if err != nil {
		utils.ReturnError(c, http.StatusInternalServerError, msg)
	} else {
		utils.ReturnOk(c, msg)
	}
}
