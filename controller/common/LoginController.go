package common

/* @Author ly
** @Date 2024/5/18 8:51:00
** @Desc
 */

import (
	"gin/service/common"
	"gin/utils"
	"github.com/gin-gonic/gin"
	"net/http"
)

type LoginController struct{}

func (l LoginController) Login(c *gin.Context) {
	token, msg := common.Login(c)
	if msg == "" {
		utils.ReturnData(c, token)
	} else {
		json := map[string]interface{}{
			"code": http.StatusOK,
			"msg":  msg,
			"data": token,
		}
		c.JSON(http.StatusOK, &json)
	}
}
func (l LoginController) GetValid(c *gin.Context) {
	data := common.GetValid(c)
	utils.ReturnData(c, data)
}
func (l LoginController) GetCaptcha(c *gin.Context) {
	data := common.GetCaptcha()
	utils.ReturnData(c, data)
}
func (l LoginController) Logout(c *gin.Context) {
	flag, msg := common.Logout(c)
	if flag {
		utils.ReturnOk(c, msg)
	} else {
		utils.ReturnError(c, http.StatusInternalServerError, msg)
	}
}

// 获取登录用户的菜单
func (l LoginController) GetRouters(c *gin.Context) {
	data, err := common.GetRouters(c)
	if err != nil {
		utils.ReturnError(c, http.StatusForbidden, err.Error())
	} else {
		utils.ReturnData(c, data)
	}
}
