package system

/* @Author ly
** @Date 2024/5/20 11:01:00
** @Desc
 */

import (
	"gin/service/system"
	"gin/utils"
	"github.com/gin-gonic/gin"
	"net/http"
)

type UserController struct{}

// 获取人员列表
func (u UserController) GetUserList(c *gin.Context) {
	data, total := system.GetUserList(c)
	utils.ReturnSuccess(c, data, total)
}

// 获取人员详细信息
func (u UserController) GetUserInfo(c *gin.Context) {
	data := system.GetUserInfo(c)
	utils.ReturnData(c, data)
}

// 添加人员信息
func (u UserController) UserAdd(c *gin.Context) {
	flag, msg := system.UserAdd(c)
	if flag {
		utils.ReturnOk(c, msg)
	} else {
		utils.ReturnError(c, http.StatusInternalServerError, msg)
	}
}

// //修改人员信息
func (u UserController) UserModify(c *gin.Context) {
	flag, msg := system.UserModify(c)
	if flag {
		utils.ReturnOk(c, msg)
	} else {
		utils.ReturnError(c, http.StatusInternalServerError, msg)
	}
}

// 删除用户
func (u UserController) UserDel(c *gin.Context) {
	flag, msg := system.UserDel(c)
	if flag {
		utils.ReturnOk(c, msg)
	} else {
		utils.ReturnError(c, http.StatusInternalServerError, msg)
	}
}

// 修改状态
func (u UserController) ChangeStatus(c *gin.Context) {
	flag, msg := system.ChangeStatus(c)
	if flag {
		utils.ReturnOk(c, msg)
	} else {
		utils.ReturnError(c, http.StatusInternalServerError, msg)
	}
}

// 重置密码
func (u UserController) ResetPasswd(c *gin.Context) {
	flag, msg := system.ResetPasswd(c)
	if flag {
		utils.ReturnOk(c, msg)
	} else {
		utils.ReturnError(c, http.StatusInternalServerError, msg)
	}
}
func (u UserController) UserExport(c *gin.Context) {
	system.UserExport(c)
}
func (u UserController) ChangeRole(c *gin.Context) {
	flag, msg := system.ChangeRole(c)
	if flag {
		utils.ReturnOk(c, msg)
	} else {
		utils.ReturnError(c, http.StatusInternalServerError, msg)
	}
}
