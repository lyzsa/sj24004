package system

/* @Author ly
** @Date 2024/5/20 11:01:00
** @Desc
 */

import (
	"gin/service/system"
	"gin/utils"
	"github.com/gin-gonic/gin"
	"net/http"
)

type RoleController struct{}

// 获取角色列表
func (r RoleController) GetRoleList(c *gin.Context) {
	data, total := system.GetRoleList(c)
	utils.ReturnSuccess(c, data, total)
}

// 获取角色详细信息
func (r RoleController) GetRoleInfo(c *gin.Context) {
	data := system.GetRoleInfo(c)
	utils.ReturnData(c, data)
}

// 添加角色信息
func (r RoleController) RoleAdd(c *gin.Context) {
	flag, msg := system.RoleAdd(c)
	if flag {
		utils.ReturnOk(c, msg)
	} else {
		utils.ReturnError(c, http.StatusInternalServerError, msg)
	}
}

// //修改角色信息
func (r RoleController) RoleModify(c *gin.Context) {
	flag, msg := system.RoleModify(c)
	if flag {
		utils.ReturnOk(c, msg)
	} else {
		utils.ReturnError(c, http.StatusInternalServerError, msg)
	}
}

// 删除角色
func (r RoleController) RoleDel(c *gin.Context) {
	flag, msg := system.RoleDel(c)
	if flag {
		utils.ReturnOk(c, msg)
	} else {
		utils.ReturnError(c, http.StatusInternalServerError, msg)
	}
}

// 获取角色菜单树
func (r RoleController) CheckMenuId(c *gin.Context) {
	data := system.CheckMenuId(c)
	utils.ReturnData(c, data)
}
