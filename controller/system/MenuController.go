package system

/* @Author ly
** @Date 2024/5/20 9:17:00
** @Desc
 */

import (
	"gin/service/system"
	"gin/utils"
	"github.com/gin-gonic/gin"
	"net/http"
)

type MenuController struct{}

func (m MenuController) GetMenuTree(c *gin.Context) {
	data := system.GetMenuTree(c)
	utils.ReturnData(c, data)
}
func (m MenuController) GetMenuInfo(c *gin.Context) {
	data := system.GetMenuInfo(c)
	utils.ReturnData(c, data)
}
func (m MenuController) MenuAdd(c *gin.Context) {
	flag, msg := system.MenuAdd(c)
	if flag {
		utils.ReturnOk(c, msg)
	} else {
		utils.ReturnError(c, http.StatusInternalServerError, msg)
	}
}
func (m MenuController) MenuModify(c *gin.Context) {
	flag, msg := system.MenuModify(c)
	if flag {
		utils.ReturnOk(c, msg)
	} else {
		utils.ReturnError(c, http.StatusInternalServerError, msg)
	}
}
func (m MenuController) MenuDel(c *gin.Context) {
	flag, msg := system.MenuDel(c)
	if flag {
		utils.ReturnOk(c, msg)
	} else {
		utils.ReturnError(c, http.StatusInternalServerError, msg)
	}
}
