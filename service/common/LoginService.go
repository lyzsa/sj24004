package common

/* @Author ly
** @Date 2024/5/18 8:53:00
** @Desc
 */

import (
	"fmt"
	"gin/domain/common"
	"gin/domain/system"
	"gin/global"
	"gin/inits"
	system2 "gin/service/system"
	"gin/utils"
	"github.com/gin-gonic/gin"
	"time"
)

// 登录
func Login(c *gin.Context) (token string, msg string) {
	username := c.PostForm("username")
	passwd := c.PostForm("passwd")
	//id := c.PostForm("id")
	//code := c.PostForm("code")
	//result, _ := global.Redis.Get(id).Result()
	//if result != "" {
	//	flag := utils.VerifyCaptcha(id, code)
	//	if flag {
	if username != "" && passwd != "" && len([]rune(username)) > 1 {
		var user []system.User
		global.DB.Raw("select u.id,u.username,u.passwd,GROUP_CONCAT(ru.role_id) role_ids,u.status status,u.login_ip,u.login_time from user u left join user_role  ru on u.id=ru.user_id where u.username=? group by u.id", username).Scan(&user)
		flag := true
		for _, u := range user {
			if u.Username == "" {
				panic(common.UserNoExist)
			}
			if u.Username == global.Settings.JwtConfig.Super {
				var count int64
				global.DB.Raw("select count(1) from user_role  ru left join user u on ru.user_id=u.id where role_id=? and not status=1", 1).Scan(&count)
				if count > 1 && count != 0 {
					panic("成田不可登录")
				}
			}
			if utils.PasswordVerify(passwd, u.Passwd) {
				if u.Status == "1" {
					panic("对不起，您的账号：" + u.Username + "已停用")
				}
				ip := utils.GetRemoteClientIp(c.Request)
				if result, _ := global.Redis.Get(common.LoginTokenKey + fmt.Sprint(u.Id) + u.Username).Result(); result != "" {
					//如果一个参数传值了 直接登录未传值为需确认登录
					//	panic("当前账号已登录")
					msg = fmt.Sprintf("当前登录用户上次登录ip为%s，登录时间为%s", u.LoginIp, utils.Gettime(u.LoginTime))
				}
				flag = false
				SetLoginStatus(ip, u.Id)
				token = inits.GetToken(c, username, u.Id)
				global.DB.Exec("insert into login_log(username,ip,msg,logintime) values(?,?,?,?)", username, ip, "登录成功", utils.GetDate())
				return
			}
		}
		if flag {
			panic(common.UserPasswordNotMatch)
		}
	} else {
		panic(common.UserOrPasswdNotNUll)
	}
	//	} else {
	//		panic(common.CaptchaError)
	//	}
	//} else {
	//	panic(common.CaptchaExpire)
	//}
	return
}
func GetValid(c *gin.Context) (data any) {
	token := c.Request.Header.Get("token")
	path := c.Query("path")
	if token == "" {
		panic("请求未携带token")
	}
	j := inits.NewJWT()
	claims, err := j.ParserToken(token)
	if err != nil {
		panic(err)
	}
	if redistoken, _ := global.Redis.Get(common.LoginTokenKey + fmt.Sprint(claims.UserId) + claims.Name).Result(); redistoken != "" {
		if redistoken == token {
			if refresh, _ := global.Redis.Get(common.RefreshToken + fmt.Sprint(claims.UserId) + claims.Name).Result(); refresh == "" {
				if claims.Role == "1" || inits.PermissionAuth(claims.FuncArray, path) {
					data = claims
				}
				if claims.ExpiresAt-time.Now().Unix() < global.Settings.JwtConfig.RefreshTokenPeriod {
					global.Redis.Set(common.RefreshToken+fmt.Sprint(claims.UserId)+claims.Name, 1, time.Duration(2)*time.Second)
					c.Header("refresh_token", inits.GetToken(c, claims.Name, claims.UserId))
				}
			}
		} else {
			panic("token已过期")
		}
	}
	return
}

// 获取验证码
func GetCaptcha() map[string]string {
	id, base64str, _, err := utils.CreateImageCaptcha()
	data := make(map[string]string)
	if err != nil {
		panic(common.GetCaptchaError)
	}
	//_, err = global.Redis.Set(id, base64str, 3*time.Minute).Result()
	//if err != nil {
	//	panic(common.GetCaptchaBad)
	//}
	data["img"] = base64str
	data["id"] = id
	data["value"] = utils.GetCaptchaVal(id)
	return data
}
func SetLoginStatus(ip string, id int) {
	global.DB.Exec("update user set login_ip=?,login_time=? where id=?", ip, utils.GetDate(), id)
}

// 退出登录
func Logout(c *gin.Context) (flag bool, msg string) {
	claims, _ := c.Get("claims")
	id := claims.(*inits.CustomClaims).Id
	if id != "" {
		result, _ := global.Redis.Del(common.LoginTokenKey + fmt.Sprint(claims.(*inits.CustomClaims).UserId) + claims.(*inits.CustomClaims).Name).Result()
		//result, _ := global.Redis.Del(common.LoginTokenKey + id).Result()
		if result == 1 {
			flag = true
			msg = "注销成功"
			global.DB.Exec("insert into login_log(username,ip,msg,logintime) values(?,?,?,?)", claims.(*inits.CustomClaims).Name, utils.GetRemoteClientIp(c.Request), "退出成功", utils.GetDate())
		} else {
			msg = "注销失败"
			global.DB.Exec("insert into login_log(username,ip,msg,logintime) values(?,?,?,?)", claims.(*inits.CustomClaims).Name, utils.GetRemoteClientIp(c.Request), "退出失败", utils.GetDate())
		}
	} else {
		msg = "当前还未登录"
	}
	return
}

// 获取路由
func GetRouters(c *gin.Context) (menu []system.Menu, e error) {
	token := c.Request.Header.Get("token")
	if token != "" {
		j := inits.NewJWT()
		claims, err := j.ParserToken(token)
		if err != nil {
			return nil, err
		}
		if claims.Role == "1" {
			menu = system2.GetMenuTree(c)
		} else {
			global.DB.Raw("select m.menu_id,m.menu_name,m.pid,m.order_num,m.path,m.component,m.menu_type,m.url,m.remark,m.icon from user u left join user_role  ru on u.id=ru.user_id left join role_menu rm on ru.role_id=rm.role_id left join menu m on m.menu_id=rm.menu_id where u.username=? order by pid,m.order_num", claims.Name).Scan(&menu)
			if !(len(menu) == 1 && menu[0].MenuId == 0) {
				menu = system2.GetMenuTrees(menu, menu[0].Pid)
			}
		}
	}
	return
}
