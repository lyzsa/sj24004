package common

import (
	"fmt"
	"gin/domain/common"
	"gin/global"
	"gin/utils"
	"github.com/gin-gonic/gin"
	"strings"
)

/* @Author ly
** @Date 2024/5/15 8:43:00
** @Desc
 */

func List(c *gin.Context) (data map[string]interface{}) {
	var api common.Api
	err := c.ShouldBindQuery(&api)
	if err != nil {
		panic(err)
	}
	if utils.StrIsNull(api.T, api.D) {
		panic(common.ParamNoFull)
	}
	data = make(map[string]interface{})
	var (
		total int64
		val   []map[string]interface{}
	)
	if api.C == "" {
		panic(common.ParamNoFull)
	}
	sql := "select " + api.C + " from " + api.T
	sqlCount := "select count(1) from " + api.T
	if api.W != "" {
		sql += " where " + api.W
		sqlCount += " where " + api.W
	}
	if api.O != "" {
		sql += " order by " + api.O
	}
	if api.L != "" {
		sql += " limit " + api.L
	}
	if api.D == "mysql" {
		global.DB.Raw(sql).Scan(&val)
		global.DB.Raw(sqlCount).Scan(&total)
	} else {
		result, _ := global.Taos.Query(sql)
		val = global.Results(result)
		_ = global.Taos.QueryRow(sqlCount).Scan(&total)
	}
	data["data"] = val
	if api.L != "" {
		data["total"] = total
	}
	return
}
func Add(c *gin.Context) (msg string, err error) {
	var api common.Api
	err = c.ShouldBind(&api)
	if utils.StrIsNull(api.T, api.D) {
		panic(common.ParamNoFull)
	}
	sql := "insert into " + api.T
	if api.C != "" {
		array := strings.Split(api.C, ",")
		var (
			key []string
			val []string
		)
		for _, s := range array {
			vals := strings.Split(s, "=")
			key = append(key, vals[0])
			val = append(val, "'"+vals[1]+"'")
		}
		sql += "(" + strings.Join(key, ",") + ") values (" + strings.Join(val, ",") + ")"
	} else {
		panic(common.ParamNoFull)
	}
	if api.D == "mysql" {
		result := global.DB.Exec(sql)
		if result.Error != nil {
			err = result.Error
			return
		}
		msg = fmt.Sprintf("添加成功，条数为：%d", result.RowsAffected)
	} else {
		row, _ := global.Taos.Exec(sql)
		msg = fmt.Sprintf("添加成功，条数为：%d", row)
	}
	return
}
func Modify(c *gin.Context) (msg string, err error) {
	var api common.Api
	err = c.ShouldBind(&api)
	if utils.StrIsNull(api.T, api.D) {
		panic(common.ParamNoFull)
	}
	sql := "update " + api.T
	if api.C != "" {
		sql += " set " + api.C
	} else {
		panic(common.ParamNoFull)
	}
	if api.W != "" {
		sql += " where " + api.W
	}
	if api.D == "mysql" {
		result := global.DB.Exec(sql)
		if result.Error != nil {
			err = result.Error
			return
		}
		msg = fmt.Sprintf("修改成功，条数为：%d", result.RowsAffected)
	} else {
		row, _ := global.Taos.Exec(sql)
		msg = fmt.Sprintf("修改成功，条数为：%d", row)
	}
	return
}
func Del(c *gin.Context) (msg string, err error) {
	var api common.Api
	err = c.ShouldBind(&api)
	if utils.StrIsNull(api.T, api.D) {
		panic(common.ParamNoFull)
	}
	sql := "delete from " + api.T
	if api.W != "" {
		sql += " where " + api.W
	}
	result := global.DB.Exec(sql)
	if result.Error != nil {
		err = result.Error
		return
	}
	if api.D == "mysql" {
		msg = fmt.Sprintf("删除成功，条数为：%d", result.RowsAffected)
	} else {
		row, _ := global.Taos.Exec(sql)
		msg = fmt.Sprintf("删除成功，条数为：%d", row)
	}
	return
}
