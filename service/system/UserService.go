package system

/* @Author ly
** @Date 2024/1/22 14:45:00
** @Desc
 */

import (
	"fmt"
	"gin/domain/common"
	"gin/domain/system"
	"gin/global"
	"gin/utils"
	"github.com/gin-gonic/gin"
	"strings"
)

func GetUserList(c *gin.Context) (u []system.User, total int64) {
	pageSize, offset := utils.Limit(c)
	var ct common.CondTime
	if err := c.ShouldBindQuery(&ct); err != nil {
		panic(err)
	}
	var user system.User
	if err := c.ShouldBindQuery(&user); err != nil {
		panic(err)
	}
	db := global.DB.Model(&u)
	//if ct.Starttime != "" {
	//	db.Where("create_time>=?", ct.Starttime)
	//}
	//if ct.Endtime != "" {
	//	db.Where("create_time<=?", ct.Endtime)
	//}
	//if user.Username != "" {
	//	db.Where("username like ?", "%"+user.Username+"%")
	//}
	//if user.Phone != "" {
	//	db.Where("phone like ?", "%"+user.Phone+"%")
	//}
	//if user.Status != "" {
	//	db.Where("status=?", user.Status)
	//}
	db.Count(&total)
	db.Select("user.id,user.username,user.sex,user.email,user.phone,user.status,user.create_time,ru.role_id role_ids,r.role_name").Joins("left join user_role  ru on user.id=ru.user_id").Joins("left join role r on ru.role_id=r.role_id").Limit(pageSize).Offset(offset).Find(&u)
	return
}

func GetUserInfo(c *gin.Context) (u system.User) {
	id := c.Query("id")
	if utils.StrIsNull(id) {
		panic(common.ParamNoFull)
	}
	global.DB.Omit("passwd,role_ids,role_name").Find(&u, "id=?", id)
	global.DB.Raw("select GROUP_CONCAT(role_id) from user_role  where user_id=? and role_id is not null", id).Scan(&u.RoleIds)
	return
}
func UserAdd(c *gin.Context) (flag bool, msg string) {
	var u system.User
	if err := c.ShouldBind(&u); err != nil {
		msg = common.ParamFormatError
		return
	}
	if utils.StrIsNull(u.Username, u.Status, u.Passwd, c.PostForm("sex")) {
		msg = common.ParamNoFull
		return
	}
	if len([]rune(u.Username)) < 2 {
		msg = common.UserNotNUll
		return
	}
	u.CreateTime = utils.GetDate()
	if u.Username == global.Settings.JwtConfig.Super {
		msg = "不可创建超级用户"
		return
	}
	if !selectUserByUsername(u, u.Passwd) {
		msg = common.UserExist
		return
	}
	u.Passwd = utils.PasswordHash(u.Passwd)
	result := global.DB.Omit("id,role_ids，role_name,login_ip,login_time").Create(&u)
	setUserByRole(u)
	//setUserByPost(u)
	if result.Error != nil {
		msg = fmt.Sprint(result.Error)
		return
	}
	msg = fmt.Sprintf("添加成功，条数为：%d", result.RowsAffected)
	flag = true
	return
}
func UserModify(c *gin.Context) (flag bool, msg string) {
	var u system.User
	if err := c.ShouldBind(&u); err != nil {
		msg = common.ParamFormatError
		return
	}
	if utils.StrIsNull(u.Status, c.PostForm("sex"), c.PostForm("id")) {
		//if utils.StrIsNull(u.Username, u.Status, c.PostForm("sex"), c.PostForm("id")) {
		msg = common.ParamNoFull
		return
	}
	if len([]rune(u.Username)) < 2 {
		msg = common.UserNotNUll
		return
	}
	selectIsAdmin(u.Id)
	//if !selectUserByUsername(u, "") {
	//	msg = common.UserExist
	//	return
	//}
	result := global.DB.Omit("id,role_ids,role_name,login_ip,login_time").Where("id=?", u.Id).Updates(&u)
	setUserByRole(u)
	//setUserByPost(u)
	if result.Error != nil {
		msg = fmt.Sprint(result.Error)
		return
	}
	msg = fmt.Sprintf("修改成功，条数为：%d", result.RowsAffected)
	flag = true
	return
}
func UserDel(c *gin.Context) (flag bool, msg string) {
	ids := c.PostForm("ids")
	if utils.StrIsNull(ids) {
		msg = common.ParamNoFull
		return
	}
	idArray := utils.StrSplitConn(ids)
	for i := 0; i < len(idArray); i++ {
		var u system.User
		u.Id = idArray[i]
		selectIsAdmin(u.Id)
		//setUserByPost(u)
		setUserByRole(u)
	}
	result := global.DB.Delete(&system.User{}, idArray)
	if result.Error != nil {
		msg = fmt.Sprint(result.Error)
		return
	}
	msg = fmt.Sprintf("删除成功，条数为：%d", result.RowsAffected)
	flag = true
	return
}
func ChangeStatus(c *gin.Context) (flag bool, msg string) {
	var u system.User
	if err := c.ShouldBind(&u); err != nil {
		msg = common.ParamFormatError
		return
	}
	if utils.StrIsNull(u.Status, c.PostForm("id")) {
		msg = common.ParamNoFull
		return
	}
	selectIsAdmin(u.Id)
	result := global.DB.Updates(&u)
	if result.Error != nil {
		msg = fmt.Sprint(result.Error)
		return
	}
	msg = fmt.Sprintf("修改成功，条数为：%d", result.RowsAffected)
	flag = true
	return
}

// 重置密码
func ResetPasswd(c *gin.Context) (flag bool, msg string) {
	var u system.User
	if err := c.ShouldBind(&u); err != nil {
		msg = common.ParamFormatError
		return
	}
	if utils.StrIsNull(u.Passwd, c.PostForm("id")) {
		msg = common.ParamNoFull
		return
	}
	selectIsAdmin(u.Id)
	u.Passwd = utils.PasswordHash(u.Passwd)
	if !selectUserByUsername(u, u.Passwd) {
		msg = common.UserExist
		return
	}
	result := global.DB.Omit("role_ids,role_name").Updates(&u)
	if result.Error != nil {
		msg = fmt.Sprint(result.Error)
		return
	}
	msg = fmt.Sprintf("修改成功，条数为：%d", result.RowsAffected)
	flag = true
	return
}
func UserExport(c *gin.Context) {
	var u []system.User
	pageSize, offset := utils.Limit(c)
	global.DB.Model(&u).Select("user.id,user.username,user.email,user.phone,user.sex,user.status,user.create_time").Limit(pageSize).Offset(offset).Find(&u)
	filename, filepath := utils.DownloadExcel([]string{"用户id", "用户名", "用户性别", "邮箱", "手机号", "帐号状态", "创建时间"}, []string{"Id", "Username", "Sex:0=男,1=女,2=未知", "Email", "Phone", "Status:0=正常,1=停用", "CreateTime"}, "用户管理", "用户管理.xlsx", u)
	utils.DownloadFile(c, filename, filepath)
}
func ChangeRole(c *gin.Context) (flag bool, msg string) {
	var u system.User
	if err := c.ShouldBind(&u); err != nil {
		msg = common.ParamFormatError
		return
	}
	if utils.StrIsNull(u.RoleIds, c.PostForm("id")) {
		msg = common.ParamNoFull
		return
	}
	setUserByRole(u)
	flag = true
	return
}

// 查询登录名是否已使用
func selectUserByUsername(u system.User, pwd string) bool {
	var (
		user []system.User
	)
	db := global.DB.Model(&u)
	if u.Username != "" {
		db.Where("username=?", u.Username)
	}
	if u.Id != 0 {
		db.Not("id=?", u.Id)
	}
	db.Find(&user)
	if len(user) > 0 {
		if pwd != "" && user[0].Username != "" {
			for _, s := range user {
				if utils.PasswordVerify(pwd, s.Passwd) {
					return false
				}
			}
		}
	}
	return true
}

// 给用户赋角色
func setUserByRole(u system.User) {
	var count int64
	if u.Username != "" {
		global.DB.Raw("select count(1) from user where not username=?", global.Settings.JwtConfig.Super).Scan(&count)
		if count == 0 {
			u.RoleIds = "1"
		}
	}
	global.DB.Exec("delete from user_role  where user_id=?", u.Id)
	if u.RoleIds != "" {
		ids := utils.StrSplitConn(u.RoleIds)
		var role []string
		for i := 0; i < len(ids); i++ {
			role = append(role, fmt.Sprintf("(%v,%v)", ids[i], u.Id))
		}
		global.DB.Exec("insert into user_role (role_id,user_id) values" + strings.Join(role, ","))
	}
}

// 给用户赋岗位
//func setUserByPost(u system.User) {
//	global.DB.Exec("delete from post_user where user_id=?", u.Id)
//	if u.PostIds != "" {
//		ids := utils.StrSplitConn(u.PostIds)
//		var post []string
//		for i := 0; i < len(ids); i++ {
//			post = append(post, fmt.Sprintf("(%v,%v)", ids[i], u.Id))
//		}
//		global.DB.Exec("insert into post_user(post_id,user_id) values" + strings.Join(post, ","))
//	}
//}

// 查询是否是超级管理员
func selectIsAdmin(userid int) {
	var u system.User
	global.DB.Model(&u).Where("username=?", global.Settings.JwtConfig.Super).Find(&u)
	if u.Id == userid {
		panic(common.AdminNotAllowOperate)
	}
}
