package system

/* @Author ly
** @Date 2024/5/20 8:55:00
** @Desc
 */

import (
	"fmt"
	"gin/domain/common"
	"gin/domain/system"
	"gin/global"
	"gin/utils"
	"github.com/gin-gonic/gin"
)

func GetMenuTree(c *gin.Context) (menu []system.Menu) {
	var m system.Menu
	db := global.DB.Model(&menu)
	if c != nil {
		if err := c.ShouldBindQuery(&m); err != nil {
			panic(err)
		}
		if m.MenuName != "" {
			db.Where("menu_name like ?", "%"+m.MenuName+"%")
		}
	}
	db.Order("pid,order_num").Find(&menu)
	if !(len(menu) == 1 && menu[0].MenuId == 0 && len(menu) > 0) {
		menu = GetMenuTrees(menu, menu[0].Pid)
	}
	return
}
func GetMenuTrees(list []system.Menu, pid int) []system.Menu {
	res := make([]system.Menu, 0)
	for i, v := range list {
		if v.Pid == pid {
			list[i].Children = GetMenuTrees(list, v.MenuId)
			list[i].IsUse = true
			res = append(res, list[i])
		}
	}
	for i, v := range list {
		if pid == 0 && !v.IsUse {
			list[i].IsUse = true
			res = append(res, list[i])
		}
	}
	return res
}
func GetMenuInfo(c *gin.Context) (m system.Menu) {
	id := c.Query("id")
	if utils.StrIsNull(id) {
		panic(common.ParamNoFull)
	}
	global.DB.Find(&m, "menu_id=?", id)
	return
}
func MenuAdd(c *gin.Context) (flag bool, msg string) {
	var m system.Menu
	if err := c.ShouldBind(&m); err != nil {
		msg = common.ParamFormatError
		return
	}
	if utils.StrIsNull(m.MenuName, c.PostForm("orderNum"), m.MenuType) {
		msg = common.ParamNoFull
		return
	} else {
		if m.MenuType != "F" && utils.StrIsNull(m.Path) {
			msg = common.ParamNoFull
			return
		}
	}
	if !selectMenuByMenuName(m) {
		msg = common.MenuNameExist
		return
	}
	result := global.DB.Omit("menu_id").Create(&m)
	if result.Error != nil {
		msg = fmt.Sprint(result.Error)
		return
	}
	msg = fmt.Sprintf("添加成功，条数为：%d", result.RowsAffected)
	flag = true
	return
}
func MenuModify(c *gin.Context) (flag bool, msg string) {
	var m system.Menu
	if err := c.ShouldBind(&m); err != nil {
		msg = common.ParamFormatError
		return
	}
	if utils.StrIsNull(m.MenuName, c.PostForm("orderNum"), m.MenuType) {
		msg = common.ParamNoFull
		return
	} else {
		if m.MenuType != "F" && utils.StrIsNull(m.Path) {
			msg = common.ParamNoFull
			return
		}
	}
	if m.MenuId == m.Pid {
		msg = "父级菜单不能是自己"
		return
	}
	if !selectMenuByMenuName(m) {
		msg = common.MenuNameExist
		return
	}
	result := global.DB.Where("menu_id=?", m.MenuId).Updates(&m)
	if result.Error != nil {
		msg = fmt.Sprint(result.Error)
		return
	}
	msg = fmt.Sprintf("修改成功，条数为：%d", result.RowsAffected)
	flag = true
	return
}
func MenuDel(c *gin.Context) (flag bool, msg string) {
	id := c.PostForm("id")
	if utils.StrIsNull(id) {
		msg = common.ParamNoFull
		return
	}
	var total int64
	global.DB.Raw("select count(1) from menu where pid=?", id).Scan(&total)
	if total != 0 {
		msg = "存在子菜单,不允许删除"
		return
	}
	global.DB.Raw("select count(1) from role_menu where menu_id=?", id).Scan(&total)
	if total != 0 {
		msg = "菜单已分配,不允许删除"
		return
	}
	result := global.DB.Delete(&system.Menu{}, id)
	if result.Error != nil {
		msg = fmt.Sprint(result.Error)
		return
	}
	msg = fmt.Sprintf("删除成功，条数为：%d", result.RowsAffected)
	flag = true
	return
}

// 查询菜单是否已被使用
func selectMenuByMenuName(m system.Menu) bool {
	var (
		menu  system.Menu
		count int64
	)
	db := global.DB.Model(&menu)
	if m.MenuName != "" {
		db.Where("menu_name=?", m.MenuName)
	}
	if m.MenuId != 0 {
		db.Not("menu_id=?", m.MenuId)
	}
	if m.Pid != 0 {
		db.Where("pid=?", m.Pid)
	}
	db.Count(&count)
	if count == 0 {
		return true
	}
	return false
}
