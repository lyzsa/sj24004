package system

/* @Author ly
** @Date 2024/5/20 11:50:00
** @Desc
 */

import (
	"fmt"
	"gin/domain/common"
	"gin/domain/system"
	"gin/global"
	"gin/utils"
	"github.com/gin-gonic/gin"
	"strings"
)

func GetRoleList(c *gin.Context) (r []system.Role, total int64) {
	pageSize, offset := utils.Limit(c)
	var ct common.CondTime
	if err := c.ShouldBindQuery(&ct); err != nil {
		panic(err)
	}
	var Role system.Role
	if err := c.ShouldBindQuery(&Role); err != nil {
		panic(err)
	}
	db := global.DB.Model(&r)
	if ct.Starttime != "" {
		db.Where("create_time>=?", ct.Starttime)
	}
	if ct.Endtime != "" {
		db.Where("create_time<=?", ct.Endtime)
	}
	if Role.RoleName != "" {
		db.Where("role_name like ?", "%"+Role.RoleName+"%")
	}
	if Role.RoleKey != "" {
		db.Where("role_key like ?", "%"+Role.RoleKey+"%")
	}
	db.Count(&total)
	db.Order("role_sort").Limit(pageSize).Offset(offset).Find(&r)
	return
}

func GetRoleInfo(c *gin.Context) (r system.Role) {
	id := c.Query("id")
	if utils.StrIsNull(id) {
		panic(common.ParamNoFull)
	}
	global.DB.Find(&r, "role_id=?", id)
	if r.RoleKey == global.Settings.JwtConfig.Super {
		global.DB.Raw("select GROUP_CONCAT(menu_id) from menu").Scan(&r.Permissions)
	} else {
		global.DB.Raw("select GROUP_CONCAT(menu_id) from role_menu where role_id=? and menu_id is not null", id).Scan(&r.Permissions)
	}
	return
}
func RoleAdd(c *gin.Context) (flag bool, msg string) {
	var r system.Role
	if err := c.ShouldBind(&r); err != nil {
		msg = common.ParamFormatError
		return
	}
	if utils.StrIsNull(r.RoleName, c.PostForm("roleSort"), r.RoleKey) {
		msg = common.ParamNoFull
		return
	}
	r.CreateTime = utils.GetDate()
	selectIsAdminRole(r.RoleId)
	if !selectRoleByRoleName(r) {
		msg = common.RoleExist
		return
	}
	result := global.DB.Create(&r)
	setRoleByPermis(r)
	if result.Error != nil {
		msg = fmt.Sprint(result.Error)
		return
	}
	msg = fmt.Sprintf("添加成功，条数为：%d", result.RowsAffected)
	flag = true
	return
}
func RoleModify(c *gin.Context) (flag bool, msg string) {
	var r system.Role
	if err := c.ShouldBind(&r); err != nil {
		msg = common.ParamFormatError
		return
	}
	if utils.StrIsNull(r.RoleName, c.PostForm("roleSort"), r.RoleKey) {
		msg = common.ParamNoFull
		return
	}
	if !selectRoleByRoleName(r) {
		msg = common.RoleExist
		return
	}
	selectIsAdminRole(r.RoleId)
	result := global.DB.Where("role_id=?", r.RoleId).Updates(&r)
	setRoleByPermis(r)
	if result.Error != nil {
		msg = fmt.Sprint(result.Error)
		return
	}
	msg = fmt.Sprintf("修改成功，条数为：%d", result.RowsAffected)
	flag = true
	return
}
func RoleDel(c *gin.Context) (flag bool, msg string) {
	ids := c.PostForm("ids")
	if utils.StrIsNull(ids) {
		msg = common.ParamNoFull
		return
	}
	idArray := utils.StrSplitConn(ids)
	for i := 0; i < len(idArray); i++ {
		var r system.Role
		r.RoleId = idArray[i]
		selectIsAdminRole(r.RoleId)
		setRoleByPermis(r)
	}
	result := global.DB.Delete(&system.Role{}, idArray)
	if result.Error != nil {
		msg = fmt.Sprint(result.Error)
		return
	}
	msg = fmt.Sprintf("删除成功，条数为：%d", result.RowsAffected)
	flag = true
	return
}

// 获取角色已拥有的菜单id
func CheckMenuId(c *gin.Context) (ids []int) {
	roleId := c.Query("roleId")
	if utils.StrIsNull(roleId) {
		panic(common.ParamNoFull)
	}
	if roleId == "1" {
		global.DB.Raw("select menu_id from menu ").Scan(&ids)
	} else {
		global.DB.Raw("select menu_id from role_menu where role_id=?", roleId).Scan(&ids)
	}
	return
}

// 查询角色是否已存在
func selectRoleByRoleName(r system.Role) bool {
	var (
		role  system.Role
		count int64
	)
	db := global.DB.Model(&role)
	if r.RoleName != "" {
		db.Where("role_name=?", r.RoleName)
	}
	if r.RoleKey != "" {
		db.Where("role_key=?", r.RoleKey)
	}
	if r.RoleId != 0 {
		db.Not("role_id=?", r.RoleId)
	}
	db.Count(&count)
	if count == 0 {
		return true
	}
	return false
}

// 权限赋值
func setRoleByPermis(r system.Role) {
	global.DB.Exec("delete from role_menu where role_id=?", r.RoleId)
	if r.Permissions != "" {
		ids := utils.StrSplitConn(r.Permissions)
		var perms []string
		for i := 0; i < len(ids); i++ {
			perms = append(perms, fmt.Sprintf("(%v,%v)", r.RoleId, ids[i]))
		}
		global.DB.Exec("insert into role_menu(role_id,menu_id) values" + strings.Join(perms, ","))
	}
}

// 查询是否是admin的权限字符
func selectIsAdminRole(roleId int) {
	var r system.Role
	global.DB.Model(&r).Where("role_key=?", global.Settings.JwtConfig.Super).Where("role_id=?", roleId).Find(&r)
	if r.RoleId != 0 {
		panic(common.AdminNotAllowOperate)
	}
}
