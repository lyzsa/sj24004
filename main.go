package main

import (
	"fmt"
	"gin/global"
	"gin/inits"
	"gin/router"
	"github.com/gin-gonic/gin"
)

func main() {
	//初始化配置文件
	inits.InitConfig()
	//初始化路由
	r := router.Router()
	//初始化mysql
	inits.InitMysqlDB()
	//初始化redis
	inits.InitRedis()
	//初始化tdengine
	inits.InitTaos()
	//初始化sqlserver
	//inits.InitSqlServer()
	banner := `
   _________     ____.________    _____  _______    ________________ 
 /   _____/    |    |\_____  \  /  |  | \   _  \  /  _____/   __   \
 \_____  \     |    | /  ____/ /   |  |_/  /_\  \/   __  \\____    /
 /        \/\__|    |/       \/    ^   /\  \_/   \  |__\  \  /    / 
/_______  /\________|\_______ \____   |  \_____  /\_____  / /____/  
        \/                   \/    |__|        \/       \/           `
	banner += "  gin " + gin.Version
	fmt.Println(banner)
	r.Run(fmt.Sprintf(":%d", global.Settings.Port))
}
