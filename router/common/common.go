package common

/* @Author ly
** @Date 2024/5/15 9:05:00
** @Desc
 */

import (
	"gin/controller/common"
	"github.com/gin-gonic/gin"
)

func InitCommon(e *gin.Engine) {
	//v := e.Group("").Use(inits.JWTAuth())
	v := e.Group("")
	{
		v.GET("/api/s", common.Controller{}.CommonList)
		v.POST("/api/i", common.Controller{}.CommonAdd)
		v.POST("/api/d", common.Controller{}.CommonDel)
		v.POST("/api/u", common.Controller{}.CommonModify)
	}
}
