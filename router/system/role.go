package system

/* @Author ly
** @Date 2024/5/20 11:44:00
** @Desc
 */

import (
	"gin/controller/system"
	"gin/inits"
	"github.com/gin-gonic/gin"
)

func InitRole(e *gin.Engine) {
	v := e.Group("").Use(inits.JWTAuth())
	{
		v.GET("/role/list", system.RoleController{}.GetRoleList)
		v.GET("/role/info", system.RoleController{}.GetRoleInfo)
		v.POST("/role/add", system.RoleController{}.RoleAdd)
		v.POST("/role/modify", system.RoleController{}.RoleModify)
		v.POST("/role/del", system.RoleController{}.RoleDel)
		v.GET("/role/checkMenuId", system.RoleController{}.CheckMenuId)
	}
}
