package system

/* @Author ly
** @Date 2024/5/14 9:13:00
** @Desc
 */

import (
	"gin/controller/common"
	"gin/controller/system"
	"gin/inits"
	"github.com/gin-gonic/gin"
)

func InitUser(e *gin.Engine) {
	v := e.Group("").Use(inits.JWTAuth())
	{
		v.GET("/logout", common.LoginController{}.Logout)
		v.GET("/user/list", system.UserController{}.GetUserList)
		v.GET("/user/info", system.UserController{}.GetUserInfo)
		v.POST("/user/modify", system.UserController{}.UserModify)
		v.POST("/user/changeStatus", system.UserController{}.ChangeStatus)
		v.POST("/user/resetPwd", system.UserController{}.ResetPasswd)
		v.POST("/user/add", system.UserController{}.UserAdd)
		v.POST("/user/del", system.UserController{}.UserDel)
		v.GET("/user/export", system.UserController{}.UserExport)
		v.POST("/user/changeRole", system.UserController{}.ChangeRole)
	}
}
