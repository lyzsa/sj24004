package system

/* @Author ly
** @Date 2024/5/20 8:59:00
** @Desc
 */

import (
	"gin/controller/system"
	"gin/inits"
	"github.com/gin-gonic/gin"
)

func InitMenu(e *gin.Engine) {
	v := e.Group("").Use(inits.JWTAuth())
	{
		v.GET("/menu/tree", system.MenuController{}.GetMenuTree)
		v.GET("/menu/info", system.MenuController{}.GetMenuInfo)
		v.POST("/menu/modify", system.MenuController{}.MenuModify)
		v.POST("/menu/add", system.MenuController{}.MenuAdd)
		v.POST("/menu/del", system.MenuController{}.MenuDel)
	}
}
