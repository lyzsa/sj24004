package router

/* @Author ly
** @Date 2024/1/22 14:45:00
** @Desc
 */
import (
	"gin/controller/common"
	common3 "gin/domain/common"
	"gin/global"
	"gin/inits"
	common2 "gin/router/common"
	"gin/router/system"
	"github.com/gin-gonic/gin"
)

func Router() *gin.Engine {
	r := gin.Default()
	gin.SetMode(global.Settings.Mode)
	//日志使用
	r.Use(inits.Log())
	//跨域使用
	r.Use(inits.Cors())
	r.Use(inits.Recover)
	r.Use(func(c *gin.Context) {
		db, err := global.DB.DB()
		if err = db.Ping(); err != nil {
			panic(common3.DatabaseConnectFail)
		}
	})
	//不使用jwt拦截
	Unless := r.Group("")
	{
		Unless.POST("/login", common.LoginController{}.Login)
		Unless.GET("/getValid", common.LoginController{}.GetValid)
		Unless.GET("/getcaptcha", common.LoginController{}.GetCaptcha)
		Unless.GET("/getRouters", common.LoginController{}.GetRouters)
	}

	//用户管理
	system.InitUser(r)
	//公共接口
	common2.InitCommon(r)
	//菜单管理
	system.InitMenu(r)
	//角色管理
	system.InitRole(r)
	return r
}
