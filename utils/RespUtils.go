package utils

/* @Author ly
** @Date 2024/1/22 14:45:00
** @Desc
 */
import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"net/url"
)

type JsonReturn struct {
	Code  int         `json:"code"`
	Msg   string      `json:"msg"`
	Data  interface{} `json:"data,omitempty"`
	Total int64       `json:"total,omitempty"`
}

type JsonOk struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}
type JsonData struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

func ReturnSuccess(c *gin.Context, data interface{}, total int64) {
	json := &JsonReturn{Code: http.StatusOK, Msg: http.StatusText(http.StatusOK), Data: data, Total: total}
	c.JSON(http.StatusOK, json)
}
func ReturnData(c *gin.Context, data interface{}) {
	json := &JsonData{Code: http.StatusOK, Msg: http.StatusText(http.StatusOK), Data: data}
	c.JSON(http.StatusOK, json)
}
func ReturnOk(c *gin.Context, msg string) {
	json := &JsonOk{Code: http.StatusOK, Msg: msg}
	c.JSON(http.StatusOK, json)
}
func ReturnError(c *gin.Context, code int, msg string) {
	json := &JsonOk{Code: code, Msg: msg}
	c.JSON(code, json)
}
func DownloadFile(c *gin.Context, filename string, filepath string) {
	fileName := url.QueryEscape(filename)
	c.Header("Access-Control-Allow-Origin", "*")
	c.Header("AAccess-Control-Expose-Headers", "Content-Disposition")
	c.Header("Content-Disposition", fmt.Sprintf("attachment; filename*=utf-8''%s", fileName))
	c.Header("Content-Type", "application/octet-stream")
	c.File(filepath)
}
