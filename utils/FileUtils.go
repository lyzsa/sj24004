package utils

/* @Author ly
** @Date 2024/1/20 8:56:00
** @Desc
 */
import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"gin/global"
	"github.com/gin-gonic/gin"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

// 文件/文件夹生成
func FileAndDirIsExist(path string) {
	result := filepath.Ext(path)
	if result != "" {
		dirPath := filepath.Dir(path) // 提取目录路径
		// 创建目录
		err := os.MkdirAll(dirPath, 0755)
		if err != nil {
			fmt.Println("创建目录失败:", err)
			return
		}
		// 创建文件
		newFile, err := os.Create(path)
		if err != nil {
			fmt.Println("创建文件失败:", err)
			return
		}
		defer func(newFile *os.File) {
			_ = newFile.Close()
		}(newFile)
	} else {
		_, err := os.Stat(path)
		if os.IsNotExist(err) {
			err = os.MkdirAll(path, 0755)
		}
	}
}

// 上传
func Upload(c *gin.Context, fileparam string) (filepath string) {
	file, err := c.FormFile(fileparam)
	if err != nil {
		return ""
	}
	filename := strings.Split(file.Filename, ".")
	filepath = global.Settings.Static + strconv.Itoa(time.Now().Year()) + "/" + strconv.Itoa(int(time.Now().Month())) + "/"
	FileAndDirIsExist(filepath)
	filepath += GetImgName() + "." + filename[1]
	FileAndDirIsExist(filepath)
	err = c.SaveUploadedFile(file, filepath)
	if err != nil {
		return ""
	}
	return
}

// 生成图片文件名称
func GetImgName() (randomString string) {
	b := make([]byte, 10)
	_, err := rand.Read(b)
	if err != nil {
		fmt.Println("生成随机字符串时发生错误:", err)
		return ""
	}
	randomString = base64.URLEncoding.EncodeToString(b)[:10]
	return randomString
}
