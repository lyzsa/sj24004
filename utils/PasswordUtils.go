package utils

/* @Author ly
** @Date 2024/1/22 14:45:00
** @Desc
 */

import (
	"github.com/mojocn/base64Captcha"
	"golang.org/x/crypto/bcrypt"
	"image/color"
	"time"
)

var result = base64Captcha.NewMemoryStore(20240, 3*time.Minute)

// 密码加密: pwdHash
func PasswordHash(pwd string) string {
	bytes, _ := bcrypt.GenerateFromPassword([]byte(pwd), bcrypt.DefaultCost)
	return string(bytes)
}

// 密码验证: pwdVerify
func PasswordVerify(pwd, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(pwd))
	return err == nil
}

// CreateImageCaptcha 生产 验证码 返回值 id base64 answer,err
func CreateImageCaptcha() (string, string, string, error) {
	var driver base64Captcha.Driver
	//数字验证码
	driver = letterConfig()
	//数字计算验证码
	//driver = mathConfig()
	if driver == nil {
		panic("生成验证码的类型没有配置，请在yaml文件中配置完再次重试启动项目")
	}
	// 创建验证码并传入创建的类型的配置，以及存储的对象
	c := base64Captcha.NewCaptcha(driver, result)
	return c.Generate()
}

// 配置 数字 验证码
func letterConfig() *base64Captcha.DriverString {
	driverString := base64Captcha.DriverString{
		Height:          50,
		Width:           100,
		NoiseCount:      0,
		ShowLineOptions: 2 | 4,
		Length:          4,
		Source:          "1234567890",
		BgColor:         &color.RGBA{R: 3, G: 102, B: 214, A: 125},
		Fonts:           []string{"RitaSmith.ttf"},
	}
	return driverString.ConvertFonts()
}

// 配置 算数 验证码
func mathConfig() *base64Captcha.DriverMath {
	mathType := &base64Captcha.DriverMath{
		Height:          50,
		Width:           100,
		NoiseCount:      0,
		ShowLineOptions: base64Captcha.OptionShowHollowLine,
		BgColor: &color.RGBA{
			R: 40,
			G: 30,
			B: 89,
			A: 29,
		},
		Fonts: []string{"RitaSmith.ttf"},
	}
	return mathType
}

// VerifyCaptcha 验证 验证码
func VerifyCaptcha(Uuid string, Code string) bool {
	return result.Verify(Uuid, Code, true)
}

func GetCaptchaVal(Uuid string) string {
	return result.Get(Uuid, true)
}
