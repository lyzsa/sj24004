package utils

/* @Author ly
** @Date 2024/5/14 9:34:00
** @Desc
 */

import (
	"fmt"
	"gin/domain/common"
	"github.com/gin-gonic/gin"
	"net/url"
	"strconv"
	"strings"
)

// form表单转&
func EncodePostForm(postForm url.Values) string {
	var parts []string
	for key, values := range postForm {
		for _, value := range values {
			encodedKey := url.QueryEscape(key)
			encodedValue := value
			if len(encodedValue) > 100 {
				encodedValue = "TOO MANY"
			}
			parts = append(parts, fmt.Sprintf("%s=%s", encodedKey, encodedValue))
		}
	}
	return strings.Join(parts, "&")
}
func StrIsNull(str ...string) bool {
	for _, v := range str {
		if v == "" {
			return true
		}
	}
	return false
}
func StrSplitConn(str string) (idArray []int) {
	strArray := strings.Split(str, ",")
	for _, idStr := range strArray {
		id, err := strconv.Atoi(idStr) // 将字符串转换为整数类型
		if err != nil {
			fmt.Println("字符串转换为整数错误:", err)
			return
		}
		idArray = append(idArray, id) // 将整数添加到切片中
	}
	return
}

// 分页
func Limit(c *gin.Context) (pageSize, offset int) {
	if StrIsNull(c.Query("currentPage"), c.Query("pageSize")) {
		panic(common.ParamNoFull)
	}
	currentPage, _ := strconv.Atoi(c.Query("currentPage"))
	pageSize, _ = strconv.Atoi(c.Query("pageSize"))
	offset = (currentPage - 1) * pageSize
	return
}

func TimeCond(c *gin.Context) (s, e string) {
	return c.Query("starttime"), c.Query("endtime")
}
