package utils

/* @Author ly
** @Date 2024/1/20 8:57:00
** @Desc
 */

import (
	"strconv"
	"time"
)

// 年月日时分秒格式
func GetDate() string {
	s := time.Now().Format(time.DateTime)
	return s
}

// 默认时间转换
func Gettime(ts string) string {
	//"2006-01-02T15:04:05Z07:00"
	t, _ := time.Parse(time.RFC3339, ts)
	//  转化为北京时间
	year := strconv.Itoa(t.Year())
	month := ZeroFillByStr(strconv.Itoa(int(t.Month())), 2)
	day := ZeroFillByStr(strconv.Itoa(t.Day()), 2)
	hour := ZeroFillByStr(strconv.Itoa(t.Hour()), 2)
	minute := ZeroFillByStr(strconv.Itoa(t.Minute()), 2)
	second := ZeroFillByStr(strconv.Itoa(t.Second()), 2)
	return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second
}

// 补0
func ZeroFillByStr(str string, l int) string {
	result := str
	if len(str) != l {
		for i := len(str); i < l; i++ {
			result = "0" + result
		}
	}
	return result
}

// td时间转换
func GetTdtime(ts string, f int) string {
	local := time.FixedZone("CST", 3600*8)
	t, _ := time.Parse("2006-01-02 15:04:05.999 -0700 MST", ts)
	t = t.In(local)
	//  转化为北京时间
	year := strconv.Itoa(t.Year())
	month := ZeroFillByStr(strconv.Itoa(int(t.Month())), 2)
	day := ZeroFillByStr(strconv.Itoa(t.Day()), 2)
	hour := ZeroFillByStr(strconv.Itoa(t.Hour()), 2)
	minute := ZeroFillByStr(strconv.Itoa(t.Minute()), 2)
	second := ZeroFillByStr(strconv.Itoa(t.Second()), 2)
	result := year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second
	if f != 0 {
		millisecond := ZeroFillByStr(strconv.Itoa(t.Nanosecond()/1e6), 3)
		result = result + "." + millisecond
	}
	return result
}
