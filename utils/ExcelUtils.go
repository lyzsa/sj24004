package utils

/* @Author ly
** @Date 2024/5/20 16:30:00
** @Desc
 */
import (
	"fmt"
	"github.com/tealeg/xlsx"
	"reflect"
	"strconv"
	"strings"
	"time"
)

func DownloadExcel[T any](heads, key []string, sheetName string, path string, obj []T) (string, string) {
	var (
		file  *xlsx.File
		sheet *xlsx.Sheet
		row   *xlsx.Row
		cell  *xlsx.Cell
		style *xlsx.Style
		err   error
	)
	filename := GetImgName() + path
	filePath := "./res/" + strconv.Itoa(time.Now().Year()) + "/" + strconv.Itoa(int(time.Now().Month())) + "/" + filename
	FileAndDirIsExist(filePath)
	// 创建文件
	file = xlsx.NewFile()
	// 添加新工作表
	sheet, err = file.AddSheet(sheetName)
	if err != nil {
		return "", ""
	}
	// 向工作表中添加新行
	row = sheet.AddRow()
	style = xlsx.NewStyle()
	style.Font = *xlsx.NewFont(15, "Verdana")
	// 头部写入
	for _, head := range heads {
		cell = row.AddCell()
		cell.Value = head
		cell.SetStyle(style)
	}
	// 设置单元格样式
	//sheet.SetColWidth(5, 5, 60) // 设置单元格宽度 0-A 1-B 2-C
	//写入结构体需插叙语句和字段对应 且不支持随便改值
	//for i := 0; i < len(obj); i++ {
	//	sheet.Row(i+1).WriteStruct(&obj[i], len(key))
	//}
	// 主体写入数据
	for i := 0; i < len(obj); i++ {
		value := reflect.ValueOf(obj[i])
		row = sheet.AddRow()
		for z := 0; z < len(key); z++ {
			if key[z] != "" {
				name := strings.Split(key[z], ":")
				setExcelData(name, row, value.FieldByName(name[0]))
			}
		}
	}
	// 在提供的路径中将文件保存到xlsx文件
	err = file.Save(filePath)
	if err != nil {
		return "", ""
	}
	return filename, filePath
}
func setExcelData(name []string, row *xlsx.Row, value reflect.Value) {
	if len(name) > 1 {
		for j := 1; j < len(name); j++ {
			val := strings.Split(name[j], ",")
			for _, vs := range val {
				v := strings.Split(vs, "=")
				switch value.Type().String() {
				case "int":
					if v[0] == fmt.Sprintf("%d", value.Int()) {
						row.AddCell().Value = v[1]
					}
				default:
					if v[0] == fmt.Sprintf("%v", value.String()) {
						row.AddCell().Value = v[1]
					}
				}
			}
		}
	} else {
		switch value.Type().String() {
		case "int":
			row.AddCell().Value = fmt.Sprintf("%d", value.Int())
		default:
			row.AddCell().Value = fmt.Sprintf("%v", value.String())
		}
	}
}
